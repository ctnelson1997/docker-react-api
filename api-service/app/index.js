const express = require('express')
const morgan = require('morgan')

const app = express()
const port = 53705

// Debugging info.
morgan(':method :url :status :res[content-length] - :response-time ms')

// Allow CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/', (req, res) => {
  res.send(
    'Welcome! Please see the API for endpoints.'
  );
})

app.get('/helloworld', (req, res) => {
  res.send({
    'title': 'Hello World!'
  });
})

app.get('/date', (req, res) => {
  const date = new Date();
  res.send({
    'date': date.toLocaleString(),
    'ms': date.getTime()
  });
})

app.get('/author', (req, res) => {
  const date = new Date();
  res.send({
    'name': 'Cole Nelson',
    'email': 'ctnelson2@wisc.edu',
    'website': 'www.coletnelson.us'
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})