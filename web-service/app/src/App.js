import React, { useState, useEffect } from 'react';

import logo from './logo.svg';
import './App.css';

function App() {

  const [helloworld, setHelloworld] = useState({
    title: 'Unknown Message'
  });

  const [apiDate, setApiDate] = useState({
    date: 'Unknown Date',
    ms: 'Unknown Date MS'
  });

  const [author, setAuthor] = useState({
    name: 'Unknown Author',
    email: 'Unknown Email',
    website: 'Unknown Website'
  });

  useEffect(() => {
    fetch(process.env.REACT_APP_API_ROOT + '/helloworld').then(res => res.json()).then(fetchedHelloworld => setHelloworld(fetchedHelloworld));
    fetch(process.env.REACT_APP_API_ROOT + '/author').then(res => res.json()).then(fetchedAuthor => setAuthor(fetchedAuthor));
    fetch(process.env.REACT_APP_API_ROOT + '/date').then(res => res.json()).then(fetchedApiDate => setApiDate(fetchedApiDate));
    setInterval(() => fetch(process.env.REACT_APP_API_ROOT + '/date').then(res => res.json()).then(fetchedApiDate => setApiDate(fetchedApiDate)), 1000);
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>{helloworld.title}</h1>
        <h2>Welcome to the React-Docker Sandbox!</h2>
        <h3>Assembled By: {author.name}</h3>
        <h4>{apiDate.date}</h4>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
