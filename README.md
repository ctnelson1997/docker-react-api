# Docker-React-API

This template project offers a Docker-React-API integration that allows students to develop React applications with a given API in a clean, isolated environment. `api-service` would be provided as a part of the homework requirements while `web-service` would be the job of the student to complete.

## Usage

The entire application can be ran with `docker-compose up` and re-built with `docker-compose up --build`. This will start both the API (port 53705) and Web (port 53706) services. Additionally, an `.env` file is provided in the top-level directory for the specification of the API target. For most this will be `http://localhost:53705`, but older machines may differ and be output of `docker-machine ip`, e.g. `http://192.168.xxx.xxx:53705`.

To run, execute `docker-compose up` (see above for usage details). This may take several minutes. In your browser, connect to `http://localhost:53706`, or whatever is the IP of `docker-machine ip`.